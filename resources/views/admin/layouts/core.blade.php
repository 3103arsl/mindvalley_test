<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <title>
            {{ config('app.name', 'Laravel') }}
        </title>
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
        @include('admin.includes.style')
        @yield('style')
        <script>
            var BASE_PATH = '{{ url(' / ') }}/admin/';
        </script>   
    </head>
    <body>
        @if(Auth::user())
        @include('admin.includes.header')
        @endif
        <div id="wrapper">
            @if(Auth::user())
            @include('admin.includes.menu') 
            @endif
            <div id="content-wrapper">
                @yield('content')
                @if(Auth::user())
                @include('admin.includes.footer')
                @endif
            </div>
        </div>
        <img style="display: none" src="{{asset('assets/core/uiblocker/images/loader.gif')}}" id="loader">
        @include('admin.includes.script')
        @yield('script')
    </body>
</html>
