@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.articles.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Show Article</h4>
                <ul>
                    <li>Name: {{ $model->title }}</li>
                    <li>Status: {{ Status::getStatus($model->status) }}</li>
                    <li>Tags: {{ $model->pluckImplodeAttribute($model->tags) }}</li>
                    <li>Description: {{ $model->description }}</li>
                    <li>Created at: {{ $model->getCreatedAt() }}</li>
                    <li>Updated at: {{ $model->getUpdatedAt() }}</li>
                    <li>Photos</li>
                    <li>@if($model && $model->photos->count()>0)
                        <div class="form-group">
                            <div class="row">
                                @foreach($model->photos as $photo)
                                <div class="col-1">
                                    <img src="{{ $photo->getImagePath() }}" width="80">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection