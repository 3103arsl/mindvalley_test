@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">
    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.articles.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Edit Article</h4>
                <div id="output"></div>
                @if($model && $model->photos->count()>0)
                <div class="form-group">
                    <div class="row">
                        @foreach($model->photos as $photo)
                        <div class="col-1">
                            <img src="{{ $photo->getImagePath() }}" width="80">
                            <br>
                            {!! Form::model($model,["route"=>array('articles.photo-destroy',$photo->id),"class"=>"general_form inline-block-form", "method"=>"DELETE","id"=>"article-photo-delete-form-".$photo->id]) !!} 
                            <a data-id="{{ $photo->id }}" class="article-photo-delete-btn"><i class="fas fa-trash "></i></a>
                            {!! Form::close() !!}
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
                {!! Form::model($model,["route"=>array('articles.update',$model->id),"class"=>"general_form", "method"=>"PUT","id"=>"article-form"]) !!} 
                @include('admin.articles.partials._form')
                <div class="form-group">
                    <button type="submit" id="article-submit-btn" class="btn btn-primary">Update</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection