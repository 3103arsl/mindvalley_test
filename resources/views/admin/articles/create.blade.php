@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.articles.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Create New Article</h4>
                <div id="output"></div>
                {!! Form::model($model,["route"=>array('articles.store'),"class"=>"general_form", "method"=>"POST","id"=>"article-form","enctype"=>"multipart/form-data"]) !!} 
                @include('admin.articles.partials._form')
                <div class="form-group">
                    <button type="submit" id="article-submit-btn" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@section('script')
<script>
    $(function () {
//        $('#tags').multiselect({
//            templates: {
//                li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
//            },
//            nonSelectedText: 'Select Tags',
//            numberDisplayed: 6,
//            selectedClass: 'bg-light',
//            buttonClass: 'form-control multiselect',
//            buttonWidth: '20%',
//            includeSelectAllOption : true,
//            onInitialized: function (select, container) {
//                container.find('input').addClass('d-none');
//            }
//
//        });
    });
</script>
@stop
@endsection