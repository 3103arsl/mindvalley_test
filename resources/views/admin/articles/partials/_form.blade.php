<div class="form-group">
    <label>Name</label>
    {!! Form::text('title',null,["class"=>"form-control validate[required]","required"=>"required"])  !!}
</div>
<div class="form-group">
    <label>Photos</label>
    {!! Form::file('photos[]',["class"=>"form-control validate[custom[validateMIME]]","id"=>"photos","multiple"=>"multiple"])  !!}
</div>
<div class="form-group">
    <label>Tags</label>
    {!! Form::select('tags[]', $tags, null,["id"=>"tags","multiple","class"=>"form-control multiselect"])  !!}
</div>
<div class="form-group">
    <label>Status</label>
    {!! Form::select('status', Status::getStatuses(), null,["class"=>"form-control"])  !!}
</div>
<div class="form-group">
    <label>Description</label>
    {!! Form::textarea('description',null,["class"=>"form-control"])  !!}
</div>