<div class="form-group">
    <label>Name</label>
    {!! Form::text('name',null,["class"=>"form-control validate[required]","required"=>"required"])  !!}
</div>
<div class="form-group">
    <label>Status</label>
    {!! Form::select('status', Status::getStatuses(), null,["class"=>"form-control"])  !!}
</div>
<div class="form-group">
    <label>Description</label>
    {!! Form::textarea('description',null,["class"=>"form-control"])  !!}
</div>