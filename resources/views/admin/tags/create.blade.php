@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.tags.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Create New Tag</h4>
                <div id="output"></div>
                {!! Form::model($model,["route"=>array('tags.store'),"class"=>"general_form", "method"=>"POST","id"=>"tag-form"]) !!} 
                @include('admin.tags.partials._form')
                <div class="form-group">
                    <button type="submit" id="tag-submit-btn" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection