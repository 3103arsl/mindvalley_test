@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.tags.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Show Tag</h4>
                <ul>
                    <li>Name: {{ $model->name }}</li>
                    <li>Status: {{ Status::getStatus($model->status) }}</li>
                    <li>Description: {{ $model->description }}</li>
                    <li>Created at: {{ $model->getCreatedAt() }}</li>
                    <li>Updated at: {{ $model->getUpdatedAt() }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection