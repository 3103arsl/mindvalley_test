@extends('admin.layouts.core')
@section('content')
<div class="container-fluid">
    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            @include('admin.tags.partials._sub-menu')
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <h4>Tags</h4>
                <div id="output"></div>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @if($models->count()>0)
                        @foreach($models as $model)
                        <tr>
                            <td>{{ $model->id }}</td>
                            <td>{{ $model->name }}</td>
                            <td>{{ Status::getStatus($model->status) }}</td>
                            <td>
                                <a href="{{ route('tags.show', $model->id) }}"><i class="fas fa-eye"></i></a>
                                | 
                                <a href="{{ route('tags.edit', $model->id) }}"><i class="fas fa-pen-square"></i></a>
                                |
                                {!! Form::model($model,["route"=>array('tags.destroy',$model->id),"class"=>"general_form inline-block-form", "method"=>"DELETE","id"=>"tag-delete-form-".$model->id]) !!} 
                                <a data-id="{{ $model->id }}" class="tag-delete-btn"><i class="fas fa-trash "></i></a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">No record found.</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection