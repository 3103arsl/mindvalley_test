<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <title>
            {{ config('app.name', 'Laravel') }}
        </title>
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
        @include('front.includes.style')
        @yield('style')
        <script>
            var BASE_PATH = '{{ url("/") }}';
        </script>   
    </head>
    <body>
        @include('front.includes.header')
        <div id="wrapper">
            <div id="content-wrapper">
                @yield('content')
                @include('front.includes.footer')
            </div>
        </div>
        @include('front.includes.script')
        @yield('script')
    </body>
</html>
