@extends('front.layouts.core')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-lg-8">
            <div class="container">
                <div class="col-md-12" id="post-data">
                    @include('front.partials._articles')
                </div>
                <input type="hidden" id="totalRecord" value="{{ $count }}">
                <input type="hidden" id="currentPage" value="{{ $articles->currentPage() }}">
            </div>

            <div class="ajax-load text-center" style="display:none">
                <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
            </div>
        </div>
        <div class="col-lg-4">
            @include('front.includes.sidebar')
        </div>
        <!-- Post Content Column -->
    </div>
    <!-- /.row -->
</div>
@endsection