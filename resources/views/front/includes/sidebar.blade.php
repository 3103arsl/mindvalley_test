
<!-- Sidebar Widgets Column -->
<div class="col-md-12">
    <!-- Categories Widget -->
    <div class="card my-4">
        <h5 class="card-header">Tags</h5>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-unstyled mb-0">
                        @if($tags)
                        @foreach($tags as $tag)
                        <li style="display: inline-block; padding-right: 10px;">
                            <a href="{{url('/tag/'.$tag->id)}}">{{ $tag->name }}</a>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>