@extends('front.layouts.core')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-lg-8">
            @if($articles->articles->count()>0)
            @foreach($articles->articles as $article)
            <div class="col-lg-12">
                <!-- Title -->
                <h1 class="mt-4"><a href="{{url('/'.$article->slug)}}">{{$article->title}}</a></h1>
                <!-- Author -->
                @if($article->creator)
                <p class="lead">
                    by
                    <a href="javascript:void(0);">{{$article->creator->getName()}}</a>
                </p>
                @endif
                <hr>
                <!-- Date/Time -->
                <p>Posted on {{ $article->getPostedOn() }}</p>
                @if($article->getFeaturedImage())
                <hr>
                <!-- Preview Image -->
                <img class="img-fluid rounded" src="{{$article->getFeaturedImage()}}" alt="{{$article->title}}">
                @endif
                @if($article->description)
                <hr>
                <!-- Post Content -->
                <p class="lead">{{ $article->description }}</p>
                @endif
            </div>
            @endforeach
            @else
            <p>No article found.</p>
            @endif
        </div>
        <div class="col-lg-4">
            @include('front.includes.sidebar')
        </div>
        <!-- Post Content Column -->




    </div>
    <!-- /.row -->
</div>
@endsection