@extends('front.layouts.core')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            @if($article)
            <div class="col-lg-12">
                <!-- Title -->
                <h1 class="mt-4"><a href="{{url('/'.$article->slug)}}">{{$article->title}}</a></h1>
                <!-- Author -->
                @if($article->creator)
                <p class="lead">
                    by
                    <a href="javascript:void(0);">{{$article->creator->getName()}}</a>
                </p>
                @endif
                <hr>
                <!-- Date/Time -->
                <p>Posted on {{ $article->getPostedOn() }}</p>
                @if($article->photos->count()>0)

                @foreach($article->photos as $photo)
                <!-- Preview Image -->
                <img width="200" class="img-fluid rounded" src="{{$photo->getImagePath()}}" alt="{{$article->title}}">
                @endforeach
                @endif
                @if($article->description)
                <hr>
                <!-- Post Content -->
                <p class="lead">{{ $article->description }}</p>
                @endif
            </div>
            @endif
        </div>
        <div class="col-lg-4">
            @include('front.includes.sidebar')
        </div>
        <!-- Post Content Column -->
    </div>
    <!-- /.row -->
</div>
@endsection