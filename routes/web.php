<?php

use App\Http\Controllers\PageController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Auth::routes(['register' => false]);
Route::get('/', ['as' => 'home', 'uses' => '\App\Http\Controllers\PageController@index']);
Route::get('/{slug}', ['as' => 'detail', 'uses' => '\App\Http\Controllers\PageController@single']);
Route::get('/tag/{id}', ['as' => 'tag', 'uses' => '\App\Http\Controllers\PageController@articlesbyTag']);



Route::group(['prefix' => 'admin'], function () {
    Route::resource('tags', '\App\Http\Controllers\Admin\TagController');
    Route::resource('articles', '\App\Http\Controllers\Admin\ArticleController');
    Route::delete('/articles/photo/{id}', ['as' => 'articles.photo-destroy', 'uses' => '\App\Http\Controllers\Admin\ArticleController@destroyPhoto']);
});


