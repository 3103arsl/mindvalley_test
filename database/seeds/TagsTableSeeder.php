<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Article;

class TagsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();
        foreach (range(1, 15) as $index) {
            DB::table('tags')->insert([
                'creator_id' => 1,
                'name' => 'Tag ' . $index,
                'description' => $faker->text,
                'status' => \App\Model\Status::STATUS_ACTIVE,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime,
            ]);
        }
    }

}
