<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ArticleTagTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create();
        foreach (range(1, 50) as $index) {
            DB::table('article_tags')->insert([
                'tag_id' => rand(1, 15),
                'article_id' => rand(1, 50),
            ]);
        }
    }

}
