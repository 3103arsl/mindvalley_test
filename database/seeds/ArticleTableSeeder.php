<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Tag;

class ArticleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();
        foreach (range(1, 50) as $index) {
            DB::table('articles')->insert([
                'creator_id' => 1,
                'title' => 'When an unknown printer ' . $index,
                'slug' => str_slug( 'When an unknown printer ' . $index),
                'description' => $faker->text,
                'status' => \App\Model\Status::STATUS_ACTIVE,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i'),
            ]);
        }
    }

}
