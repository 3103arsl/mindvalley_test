<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\ArticlePhoto;

class ArticlePhotosTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $images = [
            '0d2deb9a025eebe8cee45b37fcc1f1c6.jpg',
            '1b22cadf38484e53a2b45162768005d5.jpg',
            '1d980684c4722af96c9c155f1892fd85.jpg',
            '1ef01f0967f76bdeb074097731d6ee8d.jpg',
            'a7a45176d65706a8a7f70deabecd5747.jpg',
            'bc89ff8ca8189d764de78b46cc95d7e4.jpg',
            'bfc34b9e9108b585966e4dded3cf0148.jpg',
            'ccd7b079f90d5b5d37010b52f7064eba.jpg',
        ];
        $faker = Faker::create();
        foreach (range(1, 150) as $index) {
            $random_keys = array_rand($images, 1);

            DB::table('article_photos')->insert([
                'article_id' => rand(1, 50),
                'name' => $images[$random_keys],
                'is_featured' => 1,
                'status' => \App\Model\Status::STATUS_ACTIVE,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime,
            ]);
        }
//        $photos = ArticlePhoto::all();
//        if ($photos) {
//            foreach ($photos as $photo) {
//                $photo->name = str_replace(public_path('uploads') . '\\', '', $photo->name);
//                $photo->update();
//            }
//        }
    }

}
