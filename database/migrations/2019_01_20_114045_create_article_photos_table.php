<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlePhotosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('article_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('article_id')->unsigned()->nullable();
            $table->string('name');
            $table->tinyInteger('is_featured')->default(0)->comment('Is featured true its mean this image is first image')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0 = Inactive, 1 = Active')->nullable();
            $table->timestamps();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('article_photos');
    }

}
