<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the CategoryRequest.
     *
     * @return array
     */
    public function rules() {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    $rules = [
                        'title' => 'required',
                            //'photos' => 'mimes:jpeg,bmp,png|size:10000'
                    ];
                    if ($this->photos) {
                        $photos = count($this->photos);
                        foreach (range(0, $photos) as $index) {
                            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,bmp,png|max:10000';
                        }
                    }

                    return $rules;
                }
            case 'PUT':
            case 'PATCH': {
                    $rules = [
                        'title' => 'required',
                            //'photos' => 'mimes:jpeg,bmp,png|size:10000'
                    ];
                    if ($this->photos) {
                        $photos = count($this->photos);
                        foreach (range(0, $photos) as $index) {
                            $rules['photos.' . $index] = 'image|mimes:jpeg,jpg,bmp,png|max:10000';
                        }
                    }

                    return $rules;
                }
            default:break;
        }
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => 'Please provide article title.',
        ];
    }

}
