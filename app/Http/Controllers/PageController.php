<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Article;
use App\Models\ArticlePhoto;
use App\Model\Status;

class PageController extends Controller {

    public function index(Request $request) {
        $articles = Article::with(['tags', 'photos'])->where(['status' => Status::STATUS_ACTIVE])->orderBy('id', 'DESC')->paginate(Article::PAGINATE_COUNT);
        $count = Article::where(['status' => Status::STATUS_ACTIVE])->count();
        $count = ceil($count / Article::PAGINATE_COUNT);
        $tags = $this->getTags();
        if ($request->ajax()) {
            return view('front.partials._articles')->with([
                        'articles' => $articles,
                        'count' => $count,
            ]);
        }
        return view('front.index')->with([
                    'articles' => $articles,
                    'tags' => $tags,
                    'count' => $count,
        ]);
    }

    public function single($slug) {
        $article = Article::with(['tags', 'photos'])->where(['status' => Status::STATUS_ACTIVE, 'slug' => $slug])->first();
        $tags = $this->getTags();
        return view('front.single')->with([
                    'article' => $article,
                    'tags' => $tags
        ]);
    }

    public function articlesbyTag($id) {
        $articles = Tag::with(['articles' => function($query) {
                        $query->with(['tags', 'photos']);
                        $query->orderBy('id', 'DESC');
                    }])->where(['status' => Status::STATUS_ACTIVE, 'id' => $id])->first();
        $tags = $this->getTags();
        return view('front.tag-articles')->with([
                    'articles' => $articles,
                    'tags' => $tags
        ]);
    }

    public function getTags() {
        return Tag::where(['status' => Status::STATUS_ACTIVE])->get();
    }

}
