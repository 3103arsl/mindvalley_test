<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Tag;
use App\Models\Article;
use App\Models\ArticlePhoto;
use App\Http\Requests\ArticleRequest;
use App\Model\Status;

class ArticleController extends Controller {

    private $viewDir;
    private $model;

    public function __construct() {
        $this->viewDir = self::ADMIN_DIR . 'articles.';
    }

    public function index() {
        $models = Article::with(['tags'])->orderBy('id', 'DESC')->get();
        return view($this->viewDir . 'index')->with(compact('models'));
    }

    public function create(Article $article) {
        $model = $article;
        $tags = Tag::where(['status' => Status::STATUS_ACTIVE])->pluck('name', 'id');
        return view($this->viewDir . 'create')->with(compact('model', 'tags'));
    }

    public function store(ArticleRequest $request) {

        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $currentUser = Auth::user();
        $this->model = new Article();
        if ($this->model->saveArticle($request, $currentUser)) {
            $isPhotoSaved = ArticlePhoto::insert($this->doUpload($request->photos, ($this->model) ? $this->model->id : null));
            if (!$isPhotoSaved) {
                return response(['Good' => false, 'is_new' => true, 'Message' => 'Article created successfully. But photos not saved.']);
            }
            return response(['Good' => true, 'is_new' => true, 'Message' => 'Article created successfully.']);
        }
        return response(['Good' => false, 'is_new' => true, 'Message' => 'Article not created.']);
    }

    public function show(Article $article) {
        $model = $article->with('tags', 'photos')->find($article->id);
        return view($this->viewDir . 'show')->with(compact('model'));
    }

    public function edit(Article $article) {
        $model = $article->with('tags', 'photos')->find($article->id);
        $tags = Tag::where(['status' => Status::STATUS_ACTIVE])->pluck('name', 'id');
        return view($this->viewDir . 'edit')->with(compact('model', 'tags'));
    }

    public function update(ArticleRequest $request, Article $article) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $currentUser = Auth::user();
        $this->model = $article;
        if ($this->model->updateArticle($request, $currentUser)) {
            $isPhotoSaved = ArticlePhoto::insert($this->doUpload($request->photos, ($this->model) ? $this->model->id : null));
            if (!$isPhotoSaved) {
                return response(['Good' => false, 'is_new' => true, 'Message' => 'Article created successfully. But photos not saved.']);
            }
            return response(['Good' => true, 'is_new' => false, 'Message' => 'Article updated successfully.']);
        }
        return response(['Good' => false, 'is_new' => false, 'Message' => 'Article not updated.']);
    }

    public function destroy(Request $request, Article $article) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $article->delete();
        return response(['Good' => true, 'Message' => 'Article deleted successfully.']);
    }

    public function destroyPhoto(Request $request, $id) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $photo = ArticlePhoto::find($id);
        $photo->delete();
        return response(['Good' => true, 'Message' => 'Photo deleted successfully.']);
    }

    private function doUpload($photos, $articleId) {
        $files = [];
        if (empty($photos)) {
            return $files;
        }

        foreach ($photos as $index => $file) {
            try {
                $destinationPath = public_path(ArticlePhoto::UPLOAD_DIR);
                $fileName = str_random(25) . '.' . $file->getClientOriginalExtension();
                $file->move($destinationPath, $fileName);
                $files[] = ['name' => $fileName, 'article_id' => $articleId, 'is_featured' => ($index == 0) ? 1 : 0];
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        return $files;
    }

}
