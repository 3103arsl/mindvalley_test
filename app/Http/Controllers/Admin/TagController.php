<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Tag;
use App\Http\Requests\TagRequest;

class TagController extends Controller {

    private $viewDir;
    private $model;

    public function __construct() {
        $this->viewDir = self::ADMIN_DIR . 'tags.';
    }

    public function index() {
        $models = Tag::orderBy('id', 'DESC')->get();
        return view($this->viewDir . 'index')->with(compact('models'));
    }

    public function create(Tag $tag) {
        $model = $tag;
        return view($this->viewDir . 'create')->with(compact('model'));
    }

    public function store(TagRequest $request) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $currentUser = Auth::user();
        $this->model = new Tag();
        if ($this->model->saveTag($request, $currentUser)) {
            return response(['Good' => true, 'is_new' => true, 'Message' => 'Tag created successfully.']);
        }
        return response(['Good' => false, 'is_new' => true, 'Message' => 'Tag not created.']);
    }

    public function show(Tag $tag) {
        $model = $tag;
        return view($this->viewDir . 'show')->with(compact('model'));
    }

    public function edit(Tag $tag) {
        $model = $tag;
        return view($this->viewDir . 'edit')->with(compact('model'));
    }

    public function update(TagRequest $request, Tag $tag) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $currentUser = Auth::user();
        $this->model = $tag;
        if ($this->model->saveTag($request, $currentUser)) {
            return response(['Good' => true, 'is_new' => false, 'Message' => 'Tag updated successfully.']);
        }
        return response(['Good' => false, 'is_new' => false, 'Message' => 'Tag not updated.']);
    }

    public function destroy(Request $request, Tag $tag) {
        if (!$request->ajax()) {
            return response(['Good' => false, 'Message' => 'Invalid request.']);
        }
        $tag->delete();
        return response(['Good' => true, 'Message' => 'Tag deleted successfully.']);
    }

}
