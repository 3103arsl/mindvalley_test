<?php

namespace App\Models;

use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel {

    const SOFT_DELETES = false;
    const BASE_UPLOAD_DIR = 'uploads/';
    const PAGINATE_COUNT = 5;
    const DATE_FORMAT = 'd/m/Y';

    protected function attachedRelation($request, $relation) {
        return $this->{$relation}()->attach($request->input($relation));
    }

    protected function syncRelation($request, $relation) {
        return $this->{$relation}()->sync($request->input($relation));
    }

    protected function detachRelation() {
        return $this->{$relation}()->detach($request->input($relation));
    }

    public function pluckImplodeAttribute($model, $attribute = 'name', $glue = ', ') {
        return ($model && $model->count() > 0) ? $model->pluck($attribute)->implode($glue) : null;
    }

    public function getCount($model) {
        return $model->count();
    }

    public function getCreatedAt() {
        return $this->created_at->format(self::DATE_FORMAT);
    }
    
    public function getUpdatedAt() {
        return $this->created_at->format(self::DATE_FORMAT);
    }

    public function imploadArray($arr) {
        return (is_array($arr) && !empty($arr)) ? implode(',', $arr) : '';
    }

}
