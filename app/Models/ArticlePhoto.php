<?php

namespace App\Models;

use App\Models\Model;

class ArticlePhoto extends Model {

    const UPLOAD_DIR = 'uploads/';
    const THUMB_DIR = 'thumb/';

    protected $fillable = [
        'article_id',
        'name',
        'is_featured',
    ];

    public function article() {
        return $this->belongsTo('App\Models\Article');
    }

    public function saveArticle($request, $article, $isEdit = false) {
        $this->setData($request, $currentUser);
        if ($isEdit) {
            return $this->update();
        }
        return $this->save();
    }

    private function setData($request, $currentUser) {
        $this->article_id = ($article) ? $article->id : null;
        $this->name = $request->input('name');
        $this->is_featured = $request->input('slug');
        $this->status = $request->input('status');
        $this->description = $request->input('description');
    }

    public function getImagePath() {
        return asset(self::UPLOAD_DIR . $this->name);
    }

}
