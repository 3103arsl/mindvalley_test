<?php

namespace App\Models;

use App\Models\Model;

class Tag extends Model {

    public function articles() {
        return $this->belongsToMany('App\Models\Article', 'article_tags');
    }

    public function saveTag($request, $currentUser = null, $isEdit = false) {
        $this->setData($request, $currentUser);
        if ($isEdit) {
            return $this->update();
        }
        return $this->save();
    }

    private function setData($request, $currentUser) {
        $this->creator_id = ($currentUser) ? $currentUser->id : null;
        $this->name = $request->input('name');
        $this->status = $request->input('status');
        $this->description = $request->input('description');
    }

}
