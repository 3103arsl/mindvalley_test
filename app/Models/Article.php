<?php

namespace App\Models;

use App\Models\Model;
use App\Helpers\SlugHelper;

class Article extends Model {

    public function tags() {
        return $this->belongsToMany('App\Models\Tag', 'article_tags');
    }

    public function photos() {
        return $this->hasMany('App\Models\ArticlePhoto');
    }

    public function saveArticle($request, $currentUser = null) {
        $this->setData($request, $currentUser);
        if ($this->save()) {
            $this->attachedRelation($request, 'tags'); //Attached Tags
            return true;
        }
        return false;
    }

    public function updateArticle($request, $currentUser = null) {
        $this->setData($request, $currentUser, true);
        if ($this->update()) {
            $this->syncRelation($request, 'tags'); //Attached Tags
            return true;
        }
        return false;
    }

    private function setData($request, $currentUser, $isEdit = false) {
        $this->creator_id = ($currentUser) ? $currentUser->id : null;
        if ($isEdit) {
            $slug = $this->slug;
            if ($request->input('title') != $this->title) {
                $slug = new SlugHelper($this, $request->input('title'));
                $slug = $slug->getSlug();
            }
        } else {
            $slug = new SlugHelper($this, $request->input('title'));
            $slug = $slug->getSlug();
        }
        $this->slug = $slug;
        $this->title = $request->input('title');
        $this->status = $request->input('status');
        $this->description = $request->input('description');
    }

    public function getPostedOn() {
        return $this->created_at->format('M d, Y') . ' at ' . $this->created_at->format('h:m A');
    }

    public function getFeaturedImage() {
        if ($this->photos->count() > 0) {
            foreach ($this->photos as $photo) {
                if ($photo->is_featured) {
                    return $photo->getImagePath();
                }
            }
        }
        return false;
    }

}
