<?php

namespace App\Model;

class Status {

    const STATUS_IN_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SOFT_DELETE = 2;

    private static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_IN_ACTIVE => 'Inactive',
            //self::STATUS_SOFT_DELETE => 'Deleted',
    ];

    public static function getStatus($status = 0) {
        return (isset(self::$status[$status])) ? self::$status[$status] : null;
    }

    public static function getStatuses() {
        return self::$status;
    }

}
