## Installation

- Download the code from this Repository.
- Create database
- Goto project directory
- Generate ".env" file by copy the .env.example file and enter your database name, database username and password

## Run Commands

- Run command line and run the following command
- composer update
- php artisan key:generate
- php artisan migrate:fresh --seed

## Admin Panel
- Admin link (/mindvalley_test/public/login)
- Username: admin@gmail.com
- Password: secret

## Frontend
- (/mindvalley_test/public/)
