FILESIZE = 5242880;
OUTPUT = '#output';

function getFormAction(form) {
    return form.attr('action');
}
function getFormData(form) {
    return form.serialize();
}

function submitForm(form, callback) {
    var form = $(form);
    var action = getFormAction(form);
    if (!form.validationEngine('validate')) {
        return false;
    }
    uiBlocker();
    postRequest(action, getFormData(form), callback);
}

function submitFormWithFile(form, callback) {
    var _form = $(form);
    if (!_form.validationEngine('validate')) {
        return false;
    }
    uiBlocker();
    var fd = new FormData(_form[0]);
    $.ajax({
        url: getFormAction(_form),
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: callback,
        error: error,
        always: function () {
            uiUnBlocker();
        },
        complete:function(){
            uiUnBlocker();
        }
    });
}

function postRequest(url, data, callback) { //Post Request
    $.post(url, data, callback)
            .done(function (msg) {
                console.log("Request Completed.");
                uiUnBlocker();
            })
            .fail(error);
}
function getRequest(url, data, callback) { //Get Request
    $.get(url, data, callback)
            .done(function (msg) {
                console.log("Request Completed.");
                uiUnBlocker();
            })
            .fail(error);
}

function error(response, status, xhr) {
//    console.log("Request XHR: " + xhr);
    console.log("Request Status: " + status);
//    console.log("Error: " + response);
    getDanger(response.responseJSON.errors);
    uiUnBlocker();
}

function beforeSubmit(fileBrowser, output, isFileAttached) { //function to check file size before uploading.
    uiBlocker();
    if (!isFileAttached) {
        return false;
    }
    if (window.File && window.FileReader && window.FileList && window.Blob) {

        if (!$(fileBrowser).val()) {
            $(output).html("Are you kidding me?");
            return false;
        }

        var fsize = $(fileBrowser)[0].files[0].size; //get file size
        var ftype = $(fileBrowser)[0].files[0].type; // get file type


        //allow file types 
        switch (ftype) {
            case 'image/png':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/pjpeg':
            case 'text/plain':
            case 'text/html':
            case 'application/x-zip-compressed':
            case 'application/pdf':
            case 'application/msword':
            case 'application/vnd.ms-excel':
            case 'video/mp4':
                break;
            default:
                $(output).html("<b>" + ftype + "</b> Unsupported file type!");
                return false
        }


        if (fsize > FILESIZE) { //Allowed file size is less than 5 MB (1048576)
            $(output).html("<b>" + bytesToSize(fsize) + "</b> Too big file! <br />File is too big, it should be less than 5 MB.");
            return false
        }
        $(output).html("");
    } else { //Output error to older unsupported browsers that doesn't support HTML5 File API
        $(output).html("Please upgrade your browser, because your current browser lacks some new features we need!");
        return false;
    }
}


function OnProgress(event, position, total, percentComplete) { //progress bar function
    //Progress bar
    $('#progressbox').show();
    $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $('#statustxt').html(percentComplete + '%'); //update status text
    if (percentComplete > 50) {
        $('#statustxt').css('color', '#000'); //change status text to white after 50%
    }
}

function bytesToSize(bytes) { //function to format bites bit.ly/19yoIPO
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0)
        return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function getLoaderImg() {
    return $("#loader").attr('src');
}
function uiBlocker() {
    var imgUrl = getLoaderImg();
    $.blockUI({
        message: '<img src="' + imgUrl + '" />',
        css: {backgroundColor: 'none', border: 'none'}
    });
}
function uiUnBlocker() {
    $.unblockUI();
}

function getDanger(mssage) {
    return showMessages(mssage, 'danger');
}
function getSuccess(mssage) {
    return showMessages(mssage, 'success');
}
function showMessages(arrayData, type) {
    if (typeof arrayData == "string") {
        showOutupt(arrayData, type);
        return arrayData;
    }
    var message = '<ul>';
    $.each(arrayData, function (index, value) {
        message += '<li>' + value + '</li>';
    });
    message += '</ul>';
    showOutupt(message, type);
    return message;
}

function showOutupt(data, type) {
    window.scrollTo(0, 0);
    $(OUTPUT).html('<div class="alert alert-' + type + '">' + data + '</div>');
    setTimeout(function () {
        $(OUTPUT).html('');
    }, 5000);
}
function formReset(form) {
    $(form)[0].reset();
}

function confirmBox(txt) {
    return confirm(txt);
}