$(function () {
    $(document).on('click', '#tag-submit-btn', function (e) {
        e.preventDefault();
        var _this = $(this);
        var _form = '#tag-form';
        submitForm(_form, function (response) {
            if (response.Good) {
                getSuccess(response.Message);
                if (response.is_new) {
                    formReset(_form);
                }
                return false;
            }
            getDanger(response.Message);
        });
    });

    $(document).on('click', '.tag-delete-btn', function (e) {
        e.preventDefault();
        if (!confirmBox('Are you sure to want delete this?')) {
            return false;
        }
        var _this = $(this);
        var id = _this.attr('data-id');
        var _form = '#tag-delete-form-' + id;
        submitForm(_form, function (response) {
            if (response.Good) {
                getSuccess(response.Message);
                _this.parent().parent().parent().remove();
                return false;
            }
            getDanger(response.Message);
        });
    });


    $(document).on('click', '#article-submit-btn', function (e) {
        e.preventDefault();
        var _this = $(this);
        var _form = '#article-form';
        submitFormWithFile(_form, function (response) {
            if (response.Good) {
                getSuccess(response.Message);
                if (response.is_new) {
                    formReset(_form);
                }

                if (!response.is_new) {
                    window.location.reload();
                }
                return false;
            }
            getDanger(response.Message);
        });
    });


    $(document).on('click', '.article-delete-btn', function (e) {
        e.preventDefault();
        if (!confirmBox('Are you sure to want delete this?')) {
            return false;
        }
        var _this = $(this);
        var id = _this.attr('data-id');
        var _form = '#article-delete-form-' + id;
        submitForm(_form, function (response) {
            if (response.Good) {
                getSuccess(response.Message);
                _this.parent().parent().parent().remove();
                return false;
            }
            getDanger(response.Message);
        });
    });


    $(document).on('click', '.article-photo-delete-btn', function (e) {
        e.preventDefault();
        if (!confirmBox('Are you sure to want delete this?')) {
            return false;
        }
        var _this = $(this);
        var id = _this.attr('data-id');
        var _form = '#article-photo-delete-form-' + id;

        submitForm(_form, function (response) {
            if (response.Good) {
                getSuccess(response.Message);
                _this.parent().parent().remove();
                return false;
            }
            getDanger(response.Message);
        });
    });
});

$(window).scroll(function () {
    var currentPage = parseInt($("#currentPage").val());
    var count = parseInt($("#totalRecord").val());
    if (($(window).scrollTop() + $(window).height() >= $(document).height()) && currentPage <= count) {
        var nextPage = currentPage + 1;
        loadMoreData(nextPage);
    }
});


function loadMoreData(nextPage) {
    $.ajax(
            {
                url: BASE_PATH + '?page=' + nextPage,
                type: "get",
                beforeSend: function ()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function (data)
            {
                $('.ajax-load').hide();
                $("#post-data").append(data);
                $("#currentPage").val(nextPage);
            })
            .fail(function (jqXHR, ajaxOptions, thrownError)
            {
                alert('server not responding...');
            });
}